
[site-url]: https://eas-framework.ml
[npm-url]: https://npmjs.com/package/@eas-framework/server
[npm-img]: https://img.shields.io/npm/dt/@eas-framework/server
[site-badge]: https://img.shields.io/badge/website-open-blue
[donate-badge]: https://img.shields.io/badge/donate-DEV-blue

# EAS Framework - Easy as Possible 

<div align="center">

![Logo](https://eas-framework.ml/EASFrameworke.png)

[![website][site-badge]][site-url]
[![npm][npm-img]][npm-url]
[![pipeline status](https://gitlab.com/eas-framework/eas-framework/badges/master/pipeline.svg)](https://gitlab.com/eas-framework/eas-framework/-/commits/master)
[![coverage report](https://gitlab.com/eas-framework/eas-framework/badges/master/coverage.svg)](https://gitlab.com/eas-framework/eas-framework/-/commits/master)
[![Latest Release](https://gitlab.com/eas-framework/eas-framework/-/badges/release.svg)](https://gitlab.com/eas-framework/eas-framework/-/releases)
</div>


## About
EAS-Framework is a Node.js framework for SSR websites, with a lot of built in features.
Is similar to ASPX, includes 🔥 hot-reloads, and ⚡ blazingly fast builds


EAS stands for Embedded Active Server

### Install
```bash
npm i @eas-framework/server
```

### What in there?
A lot of build-in components - make you write less.

- SSR syntax similar to Razor
- Built in support for SASS
- Built in support for JSX, Svelte
- Built in support for TypeScript

### Features
- 😊 Easy syntax with model-component-page style
- ✨ Easy data binding with connectors - special components built in
- 🚀 Simple and easy to use in all means!

## Docs
You can see the documentation [here](https://eas-framework.ml/docs).

**Documentation website is on build**