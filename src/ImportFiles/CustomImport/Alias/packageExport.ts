import SearchRecord from "../../../Global/SearchRecord"
import {Settings}  from '../../../MainBuild/Server';

export default function(){
    return {Settings, SearchRecord};
}