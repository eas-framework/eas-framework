import EasyFs from '../OutputInput/EasyFs';
import { Dirent } from 'fs';
import { Insert, Components, GetPlugin } from '../CompileCode/InsertModels';
import { ClearWarning } from '../OutputInput/Logger'
import { BasicSettings, DeleteInDirectory, getTypes } from './SearchFileSystem';
import ReqScript from '../ImportFiles/Script';
import StaticFiles from '../ImportFiles/StaticFiles';
import path from 'path';
import CompileState from './CompileState';
import { SessionBuild } from '../CompileCode/Session';
import { CheckDependencyChange, pageDeps } from '../OutputInput/StoreDeps';
import { ExportSettings } from '../MainBuild/SettingsTypes';
import { argv } from 'process';
import { createSiteMap } from './SiteMap';
import { extensionIs, isFileType, RemoveEndType } from './FileTypes';
import { perCompile, postCompile, perCompilePage, postCompilePage } from '../BuildInComponents';
import { PageTemplate } from '../CompileCode/ScriptTemplate';
import StringTracker from '../EasyDebug/StringTracker';

async function compileFile(filePath: string, arrayType: string[], { isDebug, hasSessionInfo, nestedPage, nestedPageData, dynamicCheck }: { isDebug?: boolean, hasSessionInfo?: SessionBuild, nestedPage?: string, nestedPageData?: string, dynamicCheck?: boolean } = {}) {
    const FullFilePath = path.join(arrayType[0], filePath), FullPathCompile = arrayType[1] + filePath + '.cjs';

    const html = await EasyFs.readFile(FullFilePath, 'utf8');
    const ExcluUrl = (nestedPage ? nestedPage + '<line>' : '') + arrayType[2] + '/' + filePath;

    const sessionInfo = hasSessionInfo ?? new SessionBuild(arrayType[2] + '/' + filePath, FullFilePath, arrayType[2], isDebug, GetPlugin("SafeDebug"));
    await sessionInfo.dependence('thisPage', FullFilePath);

    await perCompilePage(sessionInfo, FullPathCompile);
    const CompiledData = (await Insert(html, FullPathCompile, Boolean(nestedPage), nestedPageData, sessionInfo, dynamicCheck)) ?? new StringTracker();
    await postCompilePage(sessionInfo, FullPathCompile);

    if (!nestedPage && CompiledData.length) {
        await EasyFs.writeFile(FullPathCompile, CompiledData.StringWithTack(FullPathCompile));
        pageDeps.update(ExcluUrl, sessionInfo.dependencies);
    }

    return { CompiledData, sessionInfo };
}

function RequireScript(script: string) {
    return ReqScript(['Production Loader'], script, getTypes.Static, { isDebug: false, onlyPrepare: true });
}

async function FilesInFolder(arrayType: string[], path: string, state: CompileState) {
    const allInFolder = await EasyFs.readdir(arrayType[0] + path, { withFileTypes: true });

    for (const i of <Dirent[]>allInFolder) {
        const n = i.name, connect = path + n;
        if (i.isDirectory()) {
            await EasyFs.mkdir(arrayType[1] + connect);
            await FilesInFolder(arrayType, connect + '/', state);
        }
        else {
            if (isFileType(BasicSettings.pageTypesArray, n)) {
                state.addPage(connect, arrayType[2]);
                if (await CheckDependencyChange(arrayType[2] + '/' + connect)) //check if not already compile from a 'in-file' call
                    await compileFile(connect, arrayType, { dynamicCheck: !extensionIs(n, BasicSettings.pageTypes.page) });
            } else if (arrayType == getTypes.Static && isFileType(BasicSettings.ReqFileTypesArray, n)) {
                state.addImport(connect);
                await RequireScript(connect);
            } else {
                state.addFile(connect);
                await StaticFiles(connect, false);
            }
        }
    }
}

async function RequireScripts(scripts: string[]) {
    for (const path of scripts) {
        await RequireScript(path);
    }
}

async function CreateCompile(t: string, state: CompileState) {
    const types = getTypes[t];
    await DeleteInDirectory(types[1]);
    return () => FilesInFolder(types, '', state);
}

/**
 * when page call other page;
 */
export async function FastCompileInFile(path: string, arrayType: string[],  { hasSessionInfo, nestedPage, nestedPageData, dynamicCheck }: { hasSessionInfo?: SessionBuild, nestedPage?: string, nestedPageData?: string, dynamicCheck?: boolean } = {}) {
    await EasyFs.makePathReal(path, arrayType[1]);
    return await compileFile(path, arrayType, {isDebug:true, hasSessionInfo, nestedPage, nestedPageData, dynamicCheck});
}

export async function FastCompile(path: string, arrayType: string[], dynamicCheck?: boolean) {
    await FastCompileInFile(path, arrayType, {dynamicCheck});
    ClearWarning();
}

export async function compileAll(Export: ExportSettings) {
    let state = !argv.includes('rebuild') && await CompileState.checkLoad()

    if (state) return () => RequireScripts(state.scripts)
    pageDeps.clear();

    state = new CompileState()

    perCompile();

    const activateArray = [await CreateCompile(getTypes.Static[2], state), await CreateCompile(getTypes.Logs[2], state), ClearWarning];

    return async () => {
        for (const i of activateArray) {
            await i();
        }
        await createSiteMap(Export, state);
        state.export()
        pageDeps.save();
        postCompile()
    }
}